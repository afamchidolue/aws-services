install AWS CLi in your local computer
run command "aws configure" to configure the access key , secret I.D and default region
## to create an a security group
aws ec2 create-security-group --group-name my-sg --description "My security group" --vpc-id vpc-1234567890abcdef0
# to configure the firewall 
aws ec2 authorize-security-group-ingress --group-id sg-1234567890abcdef0 --protocol tcp --port 22 --cidr <ip-address>/32
# Create a keypair 
aws ec2 create-key-pair --key-name mykpcli --query 'KeyMaterial' --output text > mykpcli.pem
# to list existing subnet
aws ec2 describe-subnets
# to create the ec2instance
aws ec2 run-instances --image-id ami-0c55b159cbfafe1f0 --count 1 --instance-type t2.micro --key-name mykpcli --security-group-ids sg-1234567890abcdef0
# to create a user 
aws iam create-user --user-name myuser
# to create a group 
aws iam create-group --group-name mygroup
# add a user to group 
aws iam add-user-to-group --user-name myuser --group-name mygroup
## to create a policy 
aws iam create-policy --policy-name "ChangePassword" --policy-document '{"Version":"2012-10-17","Statement":[{"Sid":"AllowChangePassword","Effect":"Allow","Action":["iam:ChangePassword"],"Resource":"*"}]}'

vim changePwdpolicy.json     . paste the policy json file inside
aws iam create-policy --policy-name "ChangePassword" --policy-document file://changePwdpolicy.json
## List and browse AWS resources using the AWS CLI
aws ec2 describe-instances
aws iam ss
