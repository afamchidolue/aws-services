Log in to the AWS Management Console.
Click on the EC2 service.
Click on the "Launch Instance" button.
Choose an Amazon Machine Image (AMI). An AMI is a pre-configured virtual machine image that contains the necessary software to run your application. select Amazon linux
Choose an instance type. This determines the size and computing power of your instance. t2micro
Configure the instance details. Here you can select the number of instances to launch, the network settings, and other instance-level settings and also create a key pairs
Add storage. You can choose the amount and type of storage you need.
Add tags. This helps you organize and identify your instances.
Configure security groups. Security groups are virtual firewalls that control inbound and outbound traffic to your instance.
Review your instance settings and launch your instance.

## Installing docker in your remote EC2 instance
mv Downloads/dockerserver.pem ~/.ssh
chmod 400 .ssh/dockerserver.pem
ssh -i ~/.ssh/dockerserver.pem ec2-user@<ip-address>
sudo yum update -y
sudo yum install docker -y
sudo service docker start   . this starts the docker
ps aux | grep docker
sudo usermod -a -G docker ec2-user
exit the server
login back to the server
docker login
docker pull famzyactivity/repo:1.2
docker images
docker run -d -p3000:3080