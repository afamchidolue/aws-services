## install a docker compose on ec2 Instance
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
create a Docker-compose.yaml file in the java-maven app
configure my jenkinsfile to copy the docker compose file and run it on ec2.
## Create a shell script to extract multiple linux commands and run on Ec2 instance
Created a server-cmds.sh that contains mutiple linux commands
configure the jenkinsfile script to scp and run the shell script in ec2 instance. check java-maven app for the jenkinsfile
commit the java-maven-app to git lab and build in jenkins